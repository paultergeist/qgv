/***************************************************************
QGVCore Sample
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("Graphviz Viewer");

    MainWindow w;
    w.showMaximized();

    QStringList args = a.arguments();
    if (args.size() > 1)
    {
        // if filename contains spaces but was not quoted it will be split into several "arguments"
        args.removeFirst();
        QString filename = args.join(" ");
        w.openFile(filename);
    }

    w.drawGraph();
    
    return a.exec();
}
