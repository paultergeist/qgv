import sys
import subprocess
import re
import os
import urllib2
import zipfile
import platform


def launch_qt_creator_proj(project_name):
    print ("Launching " + project_name + " in Qt Creator")

    file_path = os.getcwd() + "\\" + project_name

    if platform.system() == "Windows":
        output = subprocess.call(["cmd.exe", "/K", "start", file_path])
    else:
        output = subprocess.call(["open", project_name])


def check_results(pattern, text):
    if -1 != text.find(pattern):
        print("Success")
    else:
        raise ValueError(text)


def fetch_and_checkout_branch(branch):
    print ("Fetch and checkout current branch to " + branch)

    output = subprocess.check_output(["git", "fetch"])
    output = subprocess.check_output(["git", "checkout", branch])

    pattern = "Branch " + branch + " set up to track remote branch " + branch + " from origin."

    check_results(pattern, output)


def get_submodules():
    print ("Init QGV submodules")
    output = subprocess.check_output(["git", "submodule", "init"])

    pattern = "registered for path"
    check_results(pattern, output)

    print ("Get QGV submodules")
    output = subprocess.check_output(["git", "submodule", "update"])

    pattern = "checked out"
    check_results(pattern, output)


def roll_back():
    print ("Roll back sources to initial state.")
    curr_dir = os.getcwd()
    os.chdir(os.path.dirname(os.getcwd()))
    subprocess.call(["rm", "-fr", curr_dir])
    subprocess.call(["git", "clone", "https://vmikh@bitbucket.org/paultergeist/qgv.git"])


def get_graphviz_bins():
    if platform.system() == "Windows":
        archive_filename = "graphviz-2.38.zip"
    else:
        archive_filename = "graphviz.pkg"

    print ("Downloading GraphViz binaries...")

    if platform.system() == "Windows":
        response = urllib2.urlopen("http://www.graphviz.org/pub/graphviz/stable/windows/graphviz-2.38.zip")
    else:
        response = urllib2.urlopen(
            "http://www.graphviz.org/pub/graphviz/development/macos/mountainlion/graphviz-2.39.20150720.1238.pkg")

    graphviz = open(archive_filename, "wb")
    graphviz.write(response.read())
    graphviz.close()

    if os.path.isfile(archive_filename):
        print("Success")
    else:
        raise ValueError("Can not download graphviz archive")

    if platform.system() == "Windows":
        destination = "graphviz-2.38"
        print ("Extract GraphViz binaries...")

        with zipfile.ZipFile(archive_filename, "r") as z:
            z.extractall(destination)

        if os.path.isdir(destination):
            print ("Success")
        else:
            raise ValueError("Can not extract graphviz-2.38.zip")


def main():
    try:

        branch_name = "qgvWithCompareWindow"
        project_name = "QGraphViz.pro"

        fetch_and_checkout_branch(branch_name)

        get_submodules()

        get_graphviz_bins()

        launch_qt_creator_proj(project_name)

    except ValueError as er:
        print (er.message)
        roll_back()
    except subprocess.CalledProcessError as er:
        print (er.message)
        roll_back()


if __name__ == "__main__":
    main()